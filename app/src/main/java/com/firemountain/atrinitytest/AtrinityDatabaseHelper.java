package com.firemountain.atrinitytest;

import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

class AtrinityDatabaseHelper extends SQLiteOpenHelper {

    private static AtrinityDatabaseHelper sInstance;

    private static final String DB_NAME = "atrinity";
    private static final int DB_VERSION = 1;

    static synchronized AtrinityDatabaseHelper getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new AtrinityDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private AtrinityDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE REQUESTS (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "REQUEST_ID TEXT, "
                + "REQUEST_NUMBER TEXT, "
                + "NAME TEXT, "
                + "CREATED_AT TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
