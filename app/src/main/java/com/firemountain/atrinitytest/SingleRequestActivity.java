package com.firemountain.atrinitytest;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SingleRequestActivity extends Activity{

    private static final String API_KEY = "e8e6a311d54985a067ece5a008da280a";
    private static final String LOGIN = "d_blinov";
    private static final String PASSWORD = "Passw0rd";
    private static final String OBJECT_CODE = "300";
    private static final String ACTION = "GET_INFO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_request);

        Intent intent = getIntent();
        String requestID = intent.getStringExtra("REQUEST_ID_EXTRA");

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("ApiKey", API_KEY);
        params.put("Login", LOGIN);
        params.put("Password", PASSWORD);
        params.put("ObjectCode", OBJECT_CODE);
        params.put("Action", ACTION);
        params.put("Fields[RequestID]", requestID);

        client.post("http://mobile.atrinity.ru/api/service", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                TextView textStatusText = (TextView) findViewById(R.id.statusText);
                TextView textFullName = (TextView) findViewById(R.id.fullName);
                TextView textDescription = (TextView) findViewById(R.id.description);
                EditText editTextSolutionDescription =
                        (EditText) findViewById(R.id.solutionDescription);
                TextView textCreatedAt = (TextView) findViewById(R.id.createdAt);
                TextView textSLARecoveryTime = (TextView) findViewById(R.id.sLARecoveryTime);
                TextView textActualRecoveryTime = (TextView) findViewById(R.id.actualRecoveryTime);

                try {
                    textStatusText.setText(response.getJSONObject("Request")
                            .get("StatusText").toString());
                    textFullName.setText(response.getJSONObject("Request")
                            .getJSONObject("UserContactID")
                            .getJSONObject("UserID")
                            .get("FullName").toString());
                    textDescription.setText(response.getJSONObject("Request")
                            .get("Description").toString());
                    editTextSolutionDescription.setText(response.getJSONObject("Request")
                            .get("SolutionDescription").toString());
                    textCreatedAt.setText(response.getJSONObject("Request")
                            .get("CreatedAt").toString());
                    textSLARecoveryTime.setText(response.getJSONObject("Request")
                            .get("SLARecoveryTime").toString());
                    textActualRecoveryTime.setText(response.getJSONObject("Request")
                            .get("ActualRecoveryTime").toString());

                    findViewById(R.id.loading_panel).setVisibility(View.GONE);
                    findViewById(R.id.solutionDescription).setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error,
                                  JSONObject errorResponse) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(SingleRequestActivity.this);
                builder.setTitle("Ошибка!");
                builder.setMessage("Не удается установить связь, попробуйте еще раз");

                builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm =
                            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
