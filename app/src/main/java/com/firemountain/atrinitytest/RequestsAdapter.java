package com.firemountain.atrinitytest;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

class RequestsAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> requestNumber;
    private final ArrayList<String> requestName;
    private final ArrayList<String> requestCreatedAt;

    RequestsAdapter(Activity context,
                    ArrayList<String> requestNumber,
                    ArrayList<String> requestName,
                    ArrayList<String> requestCreatedAt) {
        super(context, R.layout.single_row_view, requestNumber);
        this.context = context;
        this.requestNumber = requestNumber;
        this.requestName = requestName;
        this.requestCreatedAt = requestCreatedAt;
    }

    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.single_row_view, null, true);

        TextView textRequestNumber = (TextView) rowView.findViewById(R.id.requestNumber);
        TextView textRequestName = (TextView) rowView.findViewById(R.id.Name);
        TextView textRequestCreatedAt = (TextView) rowView.findViewById(R.id.createdAt);

        textRequestNumber.setText(requestNumber.get(position));
        textRequestName.setText(requestName.get(position));
        textRequestCreatedAt.setText(requestCreatedAt.get(position));

        return rowView;
    }

}


