package com.firemountain.atrinitytest;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class RequestsActivity extends AppCompatActivity {

    AtrinityDatabaseHelper dbHelper;

    private static final String LOG_TAG = "myLogs";
    private static final String API_KEY = "e8e6a311d54985a067ece5a008da280a";
    private static final String LOGIN = "d_blinov";
    private static final String PASSWORD = "Passw0rd";
    private static final String OBJECT_CODE = "300";
    private static final String ACTION = "GET_LIST";
    private static final String FIELDS = "3CD0E650-4B81-E511-A39A-1CC1DEAD694D";

    private static ArrayList<String> requestID;
    private static ArrayList<String> requestNumber;
    private static ArrayList<String> requestName;
    private static ArrayList<String> requestCreatedAt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);

        dbHelper = AtrinityDatabaseHelper.getInstance(this);

        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        params.put("ApiKey", API_KEY);
        params.put("Login", LOGIN);
        params.put("Password", PASSWORD);
        params.put("ObjectCode", OBJECT_CODE);
        params.put("Action", ACTION);
        params.put("Fields[FilterID]", FIELDS);

        client.post("http://mobile.atrinity.ru/api/service", params, new JsonHttpResponseHandler() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {


                Log.d(LOG_TAG, "--- Insert in REQUESTS: ---");

                for (int i = 0; i < response.length(); i++) {
                    JSONObject responseObj;
                    try {
                        responseObj = response.getJSONObject(i);
                        insertRequest(db,
                                responseObj.get("RequestID").toString(),
                                responseObj.get("RequestNumber").toString(),
                                responseObj.get("Name").toString(),
                                responseObj.get("CreatedAt").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                Log.d(LOG_TAG, "--- Rows in atrinity: ---");
                final Cursor c = db.query("REQUESTS", null, null, null, null, null, null);

                if (c.moveToFirst()) {

                    int idColIndex = c.getColumnIndex("_id");
                    int requestIDColIndex = c.getColumnIndex("REQUEST_ID");
                    int requestNumberColIndex = c.getColumnIndex("REQUEST_NUMBER");
                    int nameColIndex = c.getColumnIndex("NAME");
                    int createdAtColIndex = c.getColumnIndex("CREATED_AT");

                    requestID = new ArrayList<>();
                    requestNumber = new ArrayList<>();
                    requestName = new ArrayList<>();
                    requestCreatedAt = new ArrayList<>();

                    do {
                        requestID.add(c.getString(requestIDColIndex));
                        requestNumber.add(c.getString(requestNumberColIndex));
                        requestName.add(c.getString(nameColIndex));
                        requestCreatedAt.add(c.getString(createdAtColIndex));
                        Log.d(LOG_TAG,
                                "ID = " + c.getInt(idColIndex) +
                                        ", REQUEST_ID = " + c.getString(requestIDColIndex) +
                                        ", REQUEST_NUMBER = " + c.getString(requestNumberColIndex) +
                                        ", NAME = " + c.getString(nameColIndex) +
                                        ", CREATED_AT = " + c.getString(createdAtColIndex));
                    } while (c.moveToNext());
                } else
                    Log.d(LOG_TAG, "0 rows");
                c.close();

                Log.d(LOG_TAG, "--- Clear REQUESTS: ---");
                int clearCount = db.delete("REQUESTS", null, null);
                Log.d(LOG_TAG, "deleted rows count = " + clearCount);

                dbHelper.close();

                RequestsActivity.super.deleteDatabase("atrinity");

                final RequestsAdapter adapter = new
                        RequestsAdapter(RequestsActivity.this,
                        requestNumber,
                        requestName,
                        requestCreatedAt);

                ListView requestsListView = (ListView)findViewById(R.id.main_list_view);

                requestsListView.setAdapter(adapter);

                requestsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Intent intent = new Intent
                                (RequestsActivity.this, SingleRequestActivity.class);
                        intent.putExtra("REQUEST_ID_EXTRA", requestID.get(position));
                        startActivity(intent);
                    }
                });
                findViewById(R.id.loading_panel).setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error,
                                  JSONObject errorResponse) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(RequestsActivity.this);
                builder.setTitle("Ошибка!");
                builder.setMessage("Не удается установить связь, попробуйте еще раз");

               builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
                builder.show();
            }
        });
    }

    private static void insertRequest(SQLiteDatabase db, String requestID, String requestNumber,
                                      String name, String createdAt) {
        ContentValues requestValues = new ContentValues();
        requestValues.put("REQUEST_ID", requestID);
        requestValues.put("REQUEST_NUMBER", requestNumber);
        requestValues.put("NAME", name);
        requestValues.put("CREATED_AT", createdAt);
        long rowID = db.insert("REQUESTS", null, requestValues);
        Log.d(LOG_TAG, "row inserted, ID = " + rowID);
    }
}
